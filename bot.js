const Telegraf = require('telegraf')
const session = require('telegraf/session')
const Stage = require('telegraf/stage')
const WizardScene = require('telegraf/scenes/wizard')

const { GoogleSpreadsheet } = require('google-spreadsheet');
const doc = new GoogleSpreadsheet('1t_w8K2yBf04UTrRmFj6aCedlI8D-XLBU0BJZi7cjidE')
// const doc = new GoogleSpreadsheet('1h6LkpwYddptKYj-yF6urXB2aRra4d4ZWI4vfPlHlpEE')

const bot = new Telegraf('1098951675:AAE1zP5pBJH1WffgqqqxIN9J-lQU6AfiMtY')

const fs = require('fs')  

const executionMenu = Telegraf.Extra
  .markdown()
  .markup((m) => m.keyboard([
    m.callbackButton('بدون اجرا'),
    m.callbackButton('با اجرا')
  ]).resize().oneTime())
const conformation = Telegraf.Extra
  .markdown()
  .markup((m) => m.keyboard([
    m.callbackButton('تایید سفارش'),
    m.callbackButton('لغو سفارش')
  ]).resize().oneTime())
const portfolio = Telegraf.Extra
  .markdown()
  .markup((m) => m.keyboard([
    m.callbackButton('نمونه های اجرا شده ۵۵ در ۵۵'),
    m.callbackButton('نمونه های اجرا شده ۶۰ در ۶۰')
  ]).resize().oneTime())
const products = Telegraf.Extra
  .markdown()
  .markup((m) => m.keyboard([
    m.callbackButton('طرح های ۵۵ در ۵۵'),
    m.callbackButton('طرح های ۶۰ در ۶۰')
  ]).resize().oneTime())

async function addSpreadSheet(ctx){ 
  await doc.useServiceAccountAuth(require('./client_secret.json'))
  await doc.loadInfo()
  console.log(doc.title)
  await doc.updateProperties({ title: 'Telegram BOT customers' })
  const sheet = doc.sheetsByIndex[0]
  try {
    if(await sheet.addRow({ Full_Name: ctx.session.fullName , Phone_number: ctx.session.phoneNumber , Meter: ctx.session.estimatedDimension, Execution: ctx.session.execution, Category: ctx.session.productCategory , Product_code: ctx.session.productCode , Address: ctx.session.address })){
      ctx.reply('سفارش شما با موفقیت ثبت شد، همکارانمون تا 15 دقیقه دیگر با شما تماس خواهند گرفت')
      .then(ctx.reply('برای ثبت سفارش جدید ربات را مجددا /start کنید'))
    }
  } catch(e) {
    console.log(e)
  }
}
function flushSeasion(ctx){
  ctx.session.fullName = ''
  ctx.session.phoneNumber = ''
  ctx.session.estimatedDimension = ''
  ctx.session.execution = ''
  ctx.session.Address = ''
}
function hasNumber(myString) {
  return /\d/.test(myString); 
}
function hasOnlyNumber(string){
  return /^\d+$/.test(string)
}
function finalizing(ctx){
  ctx.reply(`نام مشتری: ${ctx.session.fullName}
  شماره تماس: ${ctx.session.phoneNumber}
  متراژ حدودی: ${ctx.session.estimatedDimension}
  چگونگی انجام پروژه: ${ctx.session.execution}
  نوع کالا: ${ctx.session.productCategory}
  کد کالا: ${ctx.session.productCode}
  آیا اطلاعات فوق را تایید میکنید؟
  `,conformation)
  return ctx.wizard.next()
}
function sendPortfolio(ctx,dimension){
  if(dimension == 55){
    ctx.reply('در حال ارسال آلبوم نمونه های انجام شده ۵۵ در ۵۵، لطفا صبر کنید')
    .then(
      ctx.replyWithChatAction('upload_photo'),
      ctx.replyWithMediaGroup([
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/1.jpg') },
          caption: '🔩نمونه اجراشده تایل گچی 55*55 طرح پانچ مربع',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/2.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ نامنظم',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/3.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ نامنظم',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/4.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ مربع',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/5.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ نامنظم',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/6.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ نامنظم همراه با آسمان مجازی',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/55/7.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی 55*55 طرح پانچ نامنظم همراه با آسمان مجازی',
          type: 'photo'
        },
      ])
    )
  } else if(dimension == 60){
    ctx.reply('در حال ارسال آلبوم نمونه های انجام شده ۶۰ در ۶۰، لطفا صبر کنید')
    .then(
      ctx.replyWithChatAction('upload_photo'),
      ctx.replyWithMediaGroup([
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/1.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح حصیری ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/2.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح پانچ نامنظم ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/3.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح حصیری همراه با آسمان مجازی ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/4.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح کبری ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/5.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح سفید برفی پروژه دبستان شاهد ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Portfolio/60/6.jpg') },
          caption: '🔩نمونه اجرا شده تایل گچی طرح سفید برفی ابعد ابعاد ۶۰ در ۶۰',
          type: 'photo'
        },
      ])
    )
  }
}
function sendProducts(ctx,dimension){
  if(dimension == 55){
    ctx.reply('در حال ارسال آلبوم طرح های ۵۵ در ۵۵، لطفا صبر کنید')
    .then(
      ctx.replyWithChatAction('upload_photo'),
      ctx.replyWithMediaGroup([
        {
          media:  { source: fs.createReadStream('images/Products/60/001.jpg') },
          caption: 'کد ۱',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/002.jpg') },
          caption: 'کد ۲',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/154.jpg') },
          caption: 'کد ۱۵۴',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/238.jpg') },
          caption: 'کد ۲۳۸',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/996.jpg') },
          caption: 'کد ۹۹۶',
          type: 'photo'
        },
      ])
      .then(
        ctx.reply('کد انگلیسی طرح مورد نظر خود را وارد کنید')
      )
    )
  } else if(dimension == 60){
    ctx.reply('در حال ارسال آلبوم طرح های ۶۰ در ۶۰، لطفا صبر کنید')
    .then(
      ctx.replyWithChatAction('upload_photo'),
      ctx.replyWithMediaGroup([
        {
          media:  { source: fs.createReadStream('images/Products/60/001.jpg') },
          caption: 'کد ۱',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/002.jpg') },
          caption: 'کد ۲',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/154.jpg') },
          caption: 'کد ۱۵۴',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/238.jpg') },
          caption: 'کد ۲۳۸',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/239.jpg') },
          caption: 'کد ۲۳۹',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/631.jpg') },
          caption: 'کد ۶۳۱',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/993.jpg') },
          caption: 'کد ۹۹۳',
          type: 'photo'
        },
        {
          media:  { source: fs.createReadStream('images/Products/60/996.jpg') },
          caption: 'کد ۹۹۶',
          type: 'photo'
        },
      ])
    )
    .then(
      ctx.reply('کد انگلیسی طرح مورد نظر خود را وارد کنید')
    )
  }
}

const superWizard = new WizardScene('super-wizard',
  (ctx) => {
    ctx.reply('نام و نام خانوادگی خود را وارد کنید'),
    ctx.wizard.next()
  },
  (ctx) => {
    console.log(ctx.update.message.text)
    if(!hasNumber(ctx.update.message.text)){
      ctx.session.fullName = ctx.update.message.text
      ctx.reply('شماره تماس خود را وارد کنید')
      ctx.wizard.next()
    } else {
      ctx.reply('نام شما فقط میتواند حاوی حروف الفبا باشد.')
    }
  },
  (ctx) => {
    if(hasOnlyNumber(ctx.update.message.text)){
      console.log(ctx.update.message.text)
      ctx.session.phoneNumber = ctx.update.message.text
      ctx.reply('متراژ حدودی پروژه را معین کنید'),
      ctx.wizard.next()
    } else {
      ctx.reply('شماره شما فقط می تواند حاوی اعداد انگلیسی باشد.')
    }
  },
  (ctx) => {
    if(hasOnlyNumber(ctx.update.message.text)){
      console.log(ctx.update.message.text)
      ctx.session.estimatedDimension = ctx.update.message.text
      ctx.reply('چگونگی انجام پروژه',executionMenu)
      ctx.wizard.next()
    } else {
      ctx.reply('متراژ حدودی شما فقط میتواند حاوی اعداد انگلیسی باشد.')
    }
  },
  (ctx) => {
    if(ctx.update.message.text == 'با اجرا' || ctx.update.message.text == 'بدون اجرا'){
      ctx.session.execution = ctx.update.message.text
      ctx.reply('آدرس یا محدوده پروژه را تعیین کنید'),
      ctx.wizard.next()
    } else {
      ctx.reply('لطفا فقط از بین گزینه های موجود (با اجرا / بدون اجرا) اننخاب کنید.')
    }
  },
  (ctx) => {
    ctx.session.address = ctx.update.message.text
    ctx.reply('برای مشاهده نمونه های اجرا شده از منو زیر استفاده کنید', portfolio);
    ctx.wizard.next()
  },
  (ctx) => {
    if(ctx.update.message.text == 'نمونه های اجرا شده ۵۵ در ۵۵'){
      sendPortfolio(ctx,55)
    } else if(ctx.update.message.text == 'نمونه های اجرا شده ۶۰ در ۶۰'){
      sendPortfolio(ctx,60)
    }
    ctx.reply('برای مشاهده طرح های موجود از منو زیر استفاده کنید', products);
    ctx.wizard.next()
  },
  (ctx) => {
    if(ctx.update.message.text == 'طرح های ۵۵ در ۵۵' ){
      sendProducts(ctx,55);
      ctx.session.productCategory = '۵۵ در ۵۵'
    } else if(ctx.update.message.text == 'طرح های ۶۰ در ۶۰'){
      ctx.session.productCategory = '۶۰ در ۶۰'
      sendProducts(ctx,60); 
    }
    ctx.wizard.next()
  },
  (ctx) => {
    if(ctx.session.productCategory == '۵۵ در ۵۵'){
      var possibleCodes = ['238','996','154','001','002']
      if(possibleCodes.includes(ctx.update.message.text.toString())){
        ctx.session.productCode = ctx.update.message.text
        finalizing(ctx);
      } else {
        ctx.reply('لطفا از بین کد های موجود فقط عدد کد را به انگلیسی وارد کنید')
      }
    } else if(ctx.session.productCategory == '۶۰ در ۶۰'){
      var possibleCodes = ['238','154','001','996','993','631','239','002']
      if(possibleCodes.includes(ctx.update.message.text.toString())){
        ctx.session.productCode = ctx.update.message.text
        finalizing(ctx)
      } else {
        ctx.reply('لطفا از بین کد های موجود فقط عدد کد را به انگلیسی وارد کنید')
      }
    }
  },
  (ctx) => {
    if(ctx.update.message.text == 'تایید سفارش'){
      addSpreadSheet(ctx)
      return ctx.scene.leave()
    } else if(ctx.update.message.text == 'لغو سفارش') {
      ctx.reply('.سفارش شما لغو شد. برای ثبت سفارش جدید بات را مجددا /start کنید ')
      flushSeasion(ctx);

      return ctx.scene.leave()
    } else {
      ctx.reply('لطفا فقط از بین گزینه های موجود یکی را انتخاب کنید')
    }
  }
)
//, { default: 'super-wizard' }
const stage = new Stage([superWizard])
bot.use(session())
bot.use(stage.middleware())
bot.help((ctx)=>{
})
bot.start((ctx) => ctx.scene.enter('super-wizard'))
bot.launch()