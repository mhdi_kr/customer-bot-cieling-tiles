const { GoogleSpreadsheet } = require('google-spreadsheet');
 
// spreadsheet key is the long id in the sheets URL
const doc = new GoogleSpreadsheet('1EFFKUi6DC-0c-NVZ3jfkyyeIBMOBvvLqd6AtwKtSAhw')

async function test(){
  // OR load directly from json file if not in secure environment
  await doc.useServiceAccountAuth(require('./client_secret.json'));
  // OR use API key -- only for read-only access to public sheets
   
  await doc.loadInfo(); // loads document properties and worksheets
  console.log(doc.title);
  await doc.updateProperties({ title: 'renamed doc' });
   
  const sheet = doc.sheetsByIndex[0]; // or use doc.sheetsById[id]
  const sheetRows = await sheet.addRow({ name: 'Larry Page', email: 'larry@google.com' });
}
test()